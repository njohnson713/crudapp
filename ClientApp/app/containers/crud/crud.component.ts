import { Component, OnInit, OnDestroy } from "@angular/core";
import { IProduct } from "../../models/product.interface";
import { ICategory } from "../../models/category.interface";
import { ISupplier } from "../../models/supplier.interface";
import { ProductService } from "../../shared/product.service";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'crud',
    templateUrl: './crud.component.html'
})

export class CrudComponent {
    products: IProduct[] = [];
    categories: ICategory[] = [];
    suppliers: ISupplier[] = [];
    formLabel: string;
    isEditMode = false;
    form: FormGroup;
    product: IProduct = {} as IProduct;

    constructor(private productService: ProductService, private formBuilder: FormBuilder) {
        this.form = formBuilder.group({
            "name": ["", Validators.required],
            "quantity": ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
            "price": ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
            "category": ["", Validators.required],
            "supplier": ["", Validators.required]
        });

        this.formLabel = "Add Product";
    }

    ngOnInit() {
        this.getProducts();

        this.productService.getCategories().subscribe(categories => {
            this.categories = categories;
        });

        this.productService.getSuppliers().subscribe(suppliers => {
            this.suppliers = suppliers;
        });
    }

    onSubmit() {
        this.product.name = this.form.controls['name'].value;
        this.product.quantity = this.form.controls['quantity'].value;
        this.product.price = this.form.controls['price'].value;
        this.product.supplierId = this.form.controls['supplier'].value;
        this.product.categoryId = this.form.controls['category'].value;
        if (this.isEditMode) {
            this.productService.editProduct(this.product).subscribe(response => {
                this.getProducts();
                this.form.reset();
            });
        } else {
            this.productService.addProduct(this.product).subscribe(response => {
                this.getProducts();
                this.form.reset();
            });
        }
    }

    cancel() {
        this.formLabel = "Add Product";
        this.isEditMode = false;
        this.product = {} as IProduct;
        this.form.get("name").setValue('');
        this.form.get("quantity").setValue('');
        this.form.get('price').setValue('');
        this.form.get('supplier').setValue(0);
        this.form.get('category').setValue(0);
    }

    edit(product: IProduct) {
        this.formLabel = "Edit Product";
        this.isEditMode = true;
        this.product = product;
        this.form.get("name").setValue(product.name);
        this.form.get("quantity").setValue(product.quantity);
        this.form.get('price').setValue(product.price);
        this.form.get('supplier').setValue(product.supplierId);
        this.form.get('category').setValue(product.categoryId);
    }

    delete(product: IProduct) {
        if (confirm("Are you sure want to delete this?")) {
            this.productService.deleteProduct(product.productId).subscribe(response => {
                this.getProducts();
                this.form.reset();
            });
        }
    }

    private getProducts() {
        this.productService.getProducts().subscribe(products => {
            this.products = products;
        });
    }
}
