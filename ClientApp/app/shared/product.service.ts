import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { IProduct } from "../models/product.interface";
import { ICategory } from "../models/category.interface";
import { ISupplier } from "../models/supplier.interface";

@Injectable()
export class ProductService {
    constructor(private http: Http) { }

    getProducts() {
        alert(this.http.get("/api/product").toString());
        return this.http.get("/api/product").map(data => <IProduct[]>data.json());
    }

    addProduct(product: IProduct) {
        return this.http.post("/api/product", product);
    }

    editProduct(product: IProduct) {
        return this.http.put('/api/product/${product.ProductId}', product);
    }

    deleteProduct(productId: number) {
        return this.http.delete('/api/product/${ProductId}');
    }

    getCategories() {
        return this.http.get("/api/category").map(data => <ICategory[]>data.json());
    }

    getSuppliers() {
        return this.http.get("/api/supplier").map(data => <ISupplier[]>data.json());
    }
}
