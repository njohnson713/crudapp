namespace AspCoreServer.Controllers
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Threading.Tasks;
  using Microsoft.AspNetCore.Mvc;
  using DataProvider;
  using Models;

  [Route("api/supplier")]
  public class SupplierController : Controller
  {
    private SupplierDataProvider supplierDataProvider;

    public SupplierController(SupplierDataProvider supplierDataProvider)
    {
      this.supplierDataProvider = supplierDataProvider;
    }

    [HttpGet]
    public async Task<IEnumerable<Supplier>> Get()
    {
      return await this.supplierDataProvider.GetSuppliers();
    }

    [HttpGet("{id}")]
    public async Task<Supplier> Get(int id)
    {
      return await this.supplierDataProvider.GetSupplier(id);
    }

    [HttpPost]
    public async Task Post([FromBody]Supplier supplier)
    {
      await this.supplierDataProvider.AddSupplier(supplier);
    }

    [HttpPut("{id}")]
    public async Task Put(int id, [FromBody]Supplier supplier)
    {
      await this.supplierDataProvider.UpdateSupplier(supplier);
    }

    [HttpDelete("{id}")]
    public async Task Delete(int id)
    {
      await this.supplierDataProvider.DeleteSupplier(id);
    }
  }
}
