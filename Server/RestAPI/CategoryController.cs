namespace AspCoreServer.Controllers
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Threading.Tasks;
  using Microsoft.AspNetCore.Mvc;
  using DataProvider;
  using Models;

  [Route("api/category")]
  public class CategoryController : Controller
  {
    private CategoryDataProvider CategoryDataProvider;

    public CategoryController(CategoryDataProvider CategoryDataProvider)
    {
      this.CategoryDataProvider = CategoryDataProvider;
    }

    [HttpGet]
    public async Task<IEnumerable<Category>> Get()
    {
      return await this.CategoryDataProvider.GetCategories();
    }

    [HttpGet("{id}")]
    public async Task<Category> Get(int id)
    {
      return await this.CategoryDataProvider.GetCategory(id);
    }

    [HttpPost]
    public async Task Post([FromBody]Category Category)
    {
      await this.CategoryDataProvider.AddCategory(Category);
    }

    [HttpPut("{id}")]
    public async Task Put(int id, [FromBody]Category Category)
    {
      await this.CategoryDataProvider.UpdateCategory(Category);
    }

    [HttpDelete("{id}")]
    public async Task Delete(int id)
    {
      await this.CategoryDataProvider.DeleteCategory(id);
    }
  }
}
