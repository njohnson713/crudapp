using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreServer.DataProvider
{
    public interface ICategoryDataProvider
    {
      Task<IEnumerable<Models.Category>> GetCategories();

      Task<Models.Category> GetCategory(int id);

      Task AddCategory(Models.Category category);

      Task UpdateCategory(Models.Category category);

      Task DeleteCategory(int id);
  }
}
