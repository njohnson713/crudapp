namespace AspCoreServer.DataProvider
{
  using System;
  using System.Collections.Generic;
  using System.Data.SqlClient;
  using System.Linq;
  using System.Threading.Tasks;
  using Models;
  using Dapper;
  using System.Data;
  public class CategoryDataProvider : ICategoryDataProvider
    {
    private readonly string connectionString = "Server=localhost;Database=Database;Integrated Security=True;";

    public async Task AddCategory(Category category)
    {
      using (var sqlConnection = new SqlConnection(connectionString))
      {
        await sqlConnection.OpenAsync();
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@CategoryId", category.CategoryId);
        dynamicParameters.Add("@CategoryName", category.CategoryName);
        await sqlConnection.ExecuteAsync(
            "AddCategory",
            dynamicParameters,
            commandType: CommandType.StoredProcedure);
      }
    }

    public async Task DeleteCategory(int id)
    {
      using (var sqlConnection = new SqlConnection(connectionString))
      {
        await sqlConnection.OpenAsync();
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@id", id);
        await sqlConnection.ExecuteAsync(
            "DeleteCategory",
            dynamicParameters,
            commandType: CommandType.StoredProcedure);
      }
    }

    public async Task<Category> GetCategory(int id)
    {
      using (var sqlConnection = new SqlConnection(connectionString))
      {
        await sqlConnection.OpenAsync();
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@id", id);
        return await sqlConnection.QuerySingleAsync<Category>(
            "GetCategory",
            dynamicParameters,
            commandType: CommandType.StoredProcedure);
      }
    }

    public async Task<IEnumerable<Category>> GetCategories()
    {
      using (var sqlConnection = new SqlConnection(connectionString))
      {
        await sqlConnection.OpenAsync();
        return await sqlConnection.QueryAsync<Category>(
            "GetCategories",
            null,
            commandType: CommandType.StoredProcedure);
      }
    }

    public async Task UpdateCategory(Category category)
    {
      using (var sqlConnection = new SqlConnection(connectionString))
      {
        await sqlConnection.OpenAsync();
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@CategoryId", category.CategoryId);
        dynamicParameters.Add("@CategoryName", category.CategoryName);
        await sqlConnection.ExecuteAsync(
            "UpdateCategory",
            dynamicParameters,
            commandType: CommandType.StoredProcedure);
      }
    }


  }
}
