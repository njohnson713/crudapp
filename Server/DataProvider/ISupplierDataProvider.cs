using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreServer.DataProvider
{
    public interface ISupplierDataProvider
    {
    Task<IEnumerable<Models.Supplier>> GetSuppliers();

    Task<Models.Supplier> GetSupplier(int id);

    Task AddSupplier(Models.Supplier supplier);

    Task UpdateSupplier(Models.Supplier supplier);

    Task DeleteSupplier(int id);
  }
}
