using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreServer.DataProvider
{
    public interface IProductDataProvider
    {
      Task<IEnumerable<Models.Product>> GetProducts();

      Task<Models.Product> GetProduct(int id);

      Task AddProduct(Models.Product product);

      Task UpdateProduct(Models.Product product);

      Task DeleteProduct(int id);
  }
}
